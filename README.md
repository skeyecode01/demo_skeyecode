# Skeyecode demo server

This is a basic example of a server interacting with the Skeyecode API.

## Setup

### Prerequisite
You need to have a registered application with Skeyecode.

### Dependencies
You need to install the following on your system :

 * python3
 * pip3
 * sqlite3
 * virtualenv

For example on Ubuntu you can use ```apt-get install python3 python3-pip sqlite3 virtualenv```

### Installation
In the cloned repo, use the following commands to install python dependencies :   

    virtualenv venv -p python3    
    source venv/bin/activate   
    pip install -r requirements.txt

Set the following parameters in config.py :     

  * CLIENT_ID and CLIENT_SECRET : you can see them in the developer portal
  * SCOPE : the plan you subscribed for
  * SERVICE_ID : the id you get from Skeyecode
  * HOSTNAME : the domain name the server will be placed. The OAuth process need a redirection URL to complete the token acquisition.

The redirect_url you set in the developer portal must match  HOSTNAME + '/callback'.

#### (Optional) Use Ngrok
If you don't have a domain name or just want to test locally you can use [ngrok](https://ngrok.com/download) to link a public URL to localhost on a given port.
Simply type ```ngrok http 5000``` to get an address like https://xxx.ngrok.io that will relay any request to localhost:5000.
5000 is the default port of flask built-in server, if you change it you'll also have to edit the PORT parameter in config.py.



#### Database
For simplicity, we use sqlite3 as a database, if you want to use another RDBMS, you just need to replace SQLALCHEMY_DATABASE_URI in config.py with a connection string like 'postgresql://user:pass@host:5432/mydatabase'

There are only two tables described in models.py : Customer and Operation, that represent the final user and the transactions you'll create from the app to be validated by the user.

Create the database :

```python manage.py db upgrade```

Start the server :

```python demo_skeyecode.py```

## Usage
### Obtain an access token
All interactions with Skeyecode API need a valid access token.     
To acquire one, click on ![get_token.png](https://bitbucket.org/repo/XXrq5eK/images/3642545852-get_token.png)   
To check everything went right click on ![test_oauth.png](https://bitbucket.org/repo/XXrq5eK/images/7938339-test_oauth.png)        
You now have a valid access_token to call Skeyecode API, stored in the server's session.

### Configure device
Install Skeyecode app on a mobile device and complete the transaction to set the PIN.    
Add a user to your database on ![users.png](https://bitbucket.org/repo/XXrq5eK/images/2786339526-users.png)  
Click on Connect to link the app with the service.   
Check that the device is ready  by using 'Check status'    

### Make a transaction   
 * Click on ![new_tx.png](https://bitbucket.org/repo/XXrq5eK/images/2693095902-new_tx.png) and fill the form (this will only create the transaction in the local database)        
 * Go to ![show_tx.png](https://bitbucket.org/repo/XXrq5eK/images/1919630905-show_tx.png)
 * Send the transaction to Skeyecode
 * Send the notification to the device
 * See the result of the validation