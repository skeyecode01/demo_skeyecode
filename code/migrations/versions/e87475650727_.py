"""empty message

Revision ID: e87475650727
Revises: None
Create Date: 2016-08-04 11:37:06.184533

"""

# revision identifiers, used by Alembic.
revision = 'e87475650727'
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.create_table('customer',
    sa.Column('identity', sa.String(length=200), nullable=False),
    sa.Column('linked', sa.Boolean(), nullable=True),
    sa.Column('linktoken', sa.String(length=8), nullable=True),
    sa.Column('linkrefreshtoken', sa.String(length=100), nullable=True),
    sa.Column('linkexpires', sa.DateTime(timezone=True), nullable=True),
    sa.PrimaryKeyConstraint('identity')
    )
    op.create_table('operation',
    sa.Column('token', sa.String(length=200), nullable=False),
    sa.Column('identity', sa.String(length=200), nullable=False),
    sa.Column('message', sa.String(length=50), nullable=True),
    sa.Column('callback', sa.String(length=200), nullable=True),
    sa.Column('expiration', sa.Integer(), nullable=True),
    sa.Column('status', sa.String(length=15), nullable=True),
    sa.ForeignKeyConstraint(['identity'], ['customer.identity'], ),
    sa.PrimaryKeyConstraint('token')
    )
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('operation')
    op.drop_table('customer')
    ### end Alembic commands ###
