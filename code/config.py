import os

class Config(object):
    TESTING = False
    DEBUG = False
    SECRET_KEY = os.urandom(24)
    SQLALCHEMY_DATABASE_URI = "sqlite:///demo_skeyecode.db"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    HOST = '0.0.0.0'
    TOKEN_EXPIR = 300
    LOG_DIR = 'logs'
    os.makedirs(LOG_DIR, exist_ok=True)
    IMG_DIR = 'static'
    os.makedirs(IMG_DIR, exist_ok=True)
    APIROOT = "https://gateway.skeyecode.com"

    SERVICE_ID = "..."
    CLIENT_ID = "..."
    CLIENT_SECRET = "..."
    SCOPE = 'Basic'
    HOSTNAME = "..."
    PORT = 5000


class DevConfig(Config):
    TESTING = True
    DEBUG = True
