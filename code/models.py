import datetime as dt

import demo

db = demo.db


class Customer(db.Model):
    identity = db.Column(db.String(200), primary_key=True)
    linked = db.Column(db.Boolean)
    pin_set = db.Column(db.Boolean)
    linktoken = db.Column(db.String(8))
    operations = db.relationship('Operation', backref='customer', cascade="delete")
    linkrefreshtoken = db.Column(db.String(100))
    linkexpires = db.Column(db.DateTime(timezone=True))

    def __init__(self, useridentity):
        self.identity = useridentity
        self.linked = False
        self.pin_set = False

    def set_linktoken(self, linktoken, linkexpires):
        self.linktoken = linktoken
        exp = dt.datetime.strptime(linkexpires, '%Y-%m-%d %H:%M:%S')
        self.linkexpires = exp
        db.session.commit()

    def set_linkrefreshtoken(self, linkrefreshtoken):
        self.linkrefreshtoken = linkrefreshtoken
        db.session.commit()

    def update_status(self, data):
        self.linked = (data['link_status'] == 'SET')
        self.pin_set = (data['pin_status'] == 'SET')
        db.session.commit()


class Operation(db.Model):
    token = db.Column(db.String(200), primary_key=True)
    identity = db.Column(db.ForeignKey('customer.identity'), nullable=False)
    message = db.Column(db.String(50))
    callback = db.Column(db.String(200))
    expiration = db.Column(db.Integer)
    status = db.Column(db.String(15))
