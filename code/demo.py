#!/usr/bin/python3

import datetime as dt
from functools import wraps
import os.path

from flask import Flask, session, url_for, request, \
    render_template, flash, redirect, jsonify
from flask_oauthlib.client import OAuth
from werkzeug.security import gen_salt
from wtforms import Form, StringField, IntegerField
from flask_sqlalchemy import SQLAlchemy
import requests


app = Flask(__name__)
app.config.from_object("config.DevConfig")
oauth = OAuth(app)
db = SQLAlchemy(app)

import models

APIROOT = app.config['APIROOT']
AUTH_URL = APIROOT + '/authorize'
TOKEN_URL = APIROOT + '/oauth/token'
OAUTH_TEST_URL = APIROOT + '/oauth/test'
LINK_CREATE_URL =  APIROOT + "/link/create"
LINK_REFRESH_URL =  APIROOT + "/link/refresh"
CHECK_URL = APIROOT + "/link/check"
TRANSACTION_URL = APIROOT + "/transaction/create"
NOTIF_URL = APIROOT + "/device/notify"


remote = oauth.remote_app( 'test',
                           consumer_key=app.config['CLIENT_ID'],
                           consumer_secret=app.config['CLIENT_SECRET'],
                           request_token_params={'scope': app.config['SCOPE']},
                           request_token_url=None, # oauth2
                           access_token_url=TOKEN_URL,
                           authorize_url=AUTH_URL,
                           base_url=APIROOT)


class TransactionForm(Form):
    identity = StringField('User identity')
    message = StringField('Transaction message')
    callback = StringField('Url callback')
    expiration = IntegerField('Seconds to validate')


##  Get an access_token for the session #############################################

# Get the authorization code by a redirection to /token
@app.route('/code')
def obtain_code():
    return remote.authorize(callback=app.config['HOSTNAME'] + url_for('obtain_token'), _external=True)

# Get access token et store it in session
@app.route('/callback')
def obtain_token():
    bearer = remote.authorized_response()
    if bearer is None:
        return request.args['error description']
    session['access_token'] = bearer['access_token']
    expir = dt.datetime.utcnow() + dt.timedelta(seconds=bearer['expires_in'])
    session['token_expiration'] = expir
    flash('token obtained')
    return render_template('layout.html')


# Decorator to add to each function that needs the access_token
def check_token(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        access_token = session.get('access_token')
        if access_token is None or session.get('token_expiration') < dt.datetime.utcnow():
            flash('You need a valid token')
            return render_template('layout.html')
        kwargs['access_token'] = access_token
        res = func(*args, **kwargs)
        return res
    return wrapper

##  Calls to OAuth-protected routes ############################################

# Just check the OAuth process
@app.route('/oauthtest')
@check_token
def test_oauth(access_token=None):
    headers = { 'Authorization' : 'Bearer {}'.format(access_token) }
    r = requests.get(OAUTH_TEST_URL, headers=headers)
    flash('OAuth process : ' + r.text)
    return render_template('layout.html')

# Create a transaction
@app.route('/transaction/manual')
@check_token
def test_manual(access_token=None):
    tx_token = request.args.get('token')
    tx = models.Operation.query.get(tx_token)

    data = { "txtoken": tx_token,
             "serviceid": app.config['SERVICE_ID'],
             "identity": tx.identity,
             "message": tx.message,
             "callback": tx.callback,
             "expiration": tx.expiration
        }

    headers = {'Authorization' : 'Bearer {}'.format(access_token),
               'Content-Type' : 'application/json'}

    r = requests.post(TRANSACTION_URL, headers=headers, json=data)
    flash('skeyecode server responded : ' + r.text)
    if r.status_code == 201:
        tx.status = 'pending'
        db.session.commit()
    return redirect(url_for('display'))

# Send a notification linked to a transaction
@app.route('/notification')
@check_token
def send_notif(access_token=None):
    data = { "txtoken": request.args.get('token'),
             "identity": request.args.get('identity') }

    headers = {'Authorization' : 'Bearer {}'.format(access_token),
               'Content-Type' : 'application/json'}

    r = requests.post(NOTIF_URL, headers=headers, json=data)
    flash('skeyecode server responded : ' + r.text)

    return redirect(url_for('display'))

# Check if a device is ready to be used for a transaction
@app.route('/user/check/<id_token>')
@check_token
def check_link(id_token, access_token=None):
    data = { "serviceid": app.config['SERVICE_ID'],
             "useridentity" : id_token }

    headers = {'Authorization' : 'Bearer {}'.format(access_token),
               'Content-Type' : 'application/json'}

    r = requests.post(CHECK_URL, headers=headers, json=data)
    if r.status_code == 200:
        c = models.Customer.query.get(id_token)
        c.update_status(r.json())
    else:
        app.logger.error('error de sk_server ' + r.text)
        flash('sk_server error : ' + r.text)
    return redirect(url_for('display_users'))

##  Transaction routes #########################################################

@app.route('/transaction/display')
def display():
    ''' Display all known transactions
    '''
    tx = models.Operation.query.all()
    return render_template('display.html', transactions=tx)


@app.route('/transaction/create', methods=['GET', 'POST'])
def create_tx():
    ''' Create a transaction in our database
    '''
    if request.method == 'GET':
        f = TransactionForm()
        c = models.Customer.query.all()
        return render_template('form.html', form=f, customers=c)
    if request.method == 'POST':
        f = TransactionForm(request.form)
        tk = gen_salt(50)
        op = models.Operation(token=tk,
                              identity=f.identity.data,
                              message=f.message.data,
                              callback=os.path.join(f.callback.data, tk),
                              expiration=f.expiration.data,
                              status='waiting')
        db.session.add(op)
        db.session.commit()
        flash('Transaction {} created'.format(tk))
        return redirect(url_for('display'))


@app.route('/transaction/callback/<tx_token>', methods=['POST'])
def transaction_result(tx_token):
    ''' What the skeyecode server will use to update transaction status
    '''
    content = request.get_json()
    app.logger.info(tx_token + ' : ' + content['validation'])
    app.logger.info(dt.datetime.now())
    op = models.Operation.query.get(tx_token)
    op.status = content['validation']
    db.session.commit()
    flash('Transaction {} finished'.format(tx_token))
    return jsonify({'validation': op.status}), 200


@app.route('/transaction/cleanup')
def delete_transactions():
    db.session.query(models.Operation).delete()
    db.session.commit()
    return redirect(url_for('display'))

#  User routes #################################################################

@app.route('/user/display')
def display_users():
    users = models.Customer.query.all()
    return render_template('display_user.html', users=users)

@app.route('/user/add/<username>')
def add_user(username):
    customer = models.Customer(username)
    db.session.add(customer)
    db.session.commit()
    return redirect(url_for('display_users'))

@app.route('/user/delete/<username>')
def delete_user(username):
    customer = models.Customer(username)
    db.session.delete(customer)
    db.session.commit()
    return redirect(url_for('display_users'))

@app.route('/user/cleanup')
def delete_users():
    db.session.query(models.Customer).delete()
    db.session.commit()
    return redirect(url_for('display_users'))

##  Connection #################################################################

@app.route('/connect/<id_token>', methods=['GET', 'POST'])
@check_token
def connect(id_token, access_token=None):
    '''
    '''
    customer = models.Customer.query.get(id_token)

    if request.method == 'GET':
        data = { "serviceid": app.config['SERVICE_ID'],
                 "useridentity" : id_token }

        app.logger.debug("/connect/ : customer.linktoken : %s", customer.linktoken)

        if customer.linktoken is None or customer.linktoken == '':

            headers = {'Authorization' : 'Bearer {}'.format(access_token),
                      'Content-Type' : 'application/json'}

            r = requests.post(LINK_CREATE_URL, headers=headers, json=data)
            print(r.status_code)
            if r.status_code == 201:
                data = r.json()
                app.logger.debug("/connect/ : create ok : %s", data)

                refresh_token = data['refreshtoken']
                link_token = data['linktoken']
                link_expires = data['expires']

                app.logger.debug("/connect/ : create refresh_token : %s", refresh_token)
                customer.set_linkrefreshtoken(refresh_token)
                customer.set_linktoken(link_token, link_expires)
                app.logger.debug("/connect/ : create : db updated")
            else:
                flash('sk_server error : {}'.format(r.status_code))
                return redirect(url_for('display_users'))

        link_token = customer.linktoken
        link_expires = customer.linkexpires

        return render_template('show_link.html', link_token=link_token, link_expires=link_expires)

    # js will call that url with a post to ask for a refreshed token
    if request.method == 'POST':
        app.logger.debug("refreshing token for : %s", id_token)

        data = { "refreshtoken": customer.linkrefreshtoken }
        headers = {'Authorization' : 'Bearer {}'.format(access_token),
                   'Content-Type' : 'application/json'}

        r = requests.post(LINK_REFRESH_URL, headers=headers, json=data)

        data = r.json()
        link_token = data['linktoken']
        link_expires = data['expires']

        customer.set_linktoken(link_token, link_expires)

        expiration_date = dt.datetime.strptime(str(link_expires),"%Y-%m-%d %H:%M:%S")
        time_to_renewal = expiration_date - dt.datetime.utcnow()

        resp = {}
        resp["linktoken"] = link_token
        resp["time_to_renewal_in_s"] = time_to_renewal.total_seconds()
        resp["expires_date"] = link_expires

        return jsonify(resp), 200

##  Utility ####################################################################

@app.route('/log')
def get_log():
    LOG_FILE = './logs/uwsgi.log'
    if os.path.isfile(LOG_FILE):
        with open(LOG_FILE) as f:
            content = f.readlines()[-50:]
    else:
        content = ['No log file']
    return render_template('log.html', content=content)

@app.route('/')
def index():
    return render_template('layout.html')


if __name__ == '__main__':
    app.run(port=app.config['PORT'])
