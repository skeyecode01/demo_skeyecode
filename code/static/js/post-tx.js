// GLOBAL
var gTxToken;

// function sendData(){

// 	var xhr = new XMLHttpRequest();
// 	var txToken;

// 	// callback when done
// 	xhr.onreadystatechange = function() {
// 		// if (xhr.readyState == 4 && xhr.status == 200) {
// 		if (xhr.readyState == 4) {
// 			console.log("response : ", xhr.responseText);
// 			document.getElementById("tx-response").innerHTML = xhr.responseText + " token : " + txToken ;
// 			gTxToken = txToken;
// 		}
// 	};

// 	xhr.open("POST", "https://api.skeyecode.com/transaction/create", true);
// 	xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

// 	// generate a random tx token
// 	txToken = Math.floor(Math.random() * 100000000);
// 	console.log("txToken : ", txToken);

// 	var form = document.getElementsByName("tx-create-form");
// 	var form0 = form[0];

// 	var message = form0.elements.namedItem("message").value;
// 	var callback = form0.elements.namedItem("callback").value;
// 	var identity = form0.elements.namedItem("identity").value;
// 	console.log("message : ", message, " callback : ", callback, " identity : ", identity);

// 	var j = {
// 		"message": message,
// 		"callback": callback,
// 		"identity": identity,
// 		"txtoken": txToken,
// 		"serviceid": "testservice"
// 	};
// 	xhr.send(JSON.stringify(j));
// }

function getDevices(){
	var xhr = new XMLHttpRequest();
	var txToken;

	// callback when done
	xhr.onreadystatechange = function() {
		// if (xhr.readyState == 4 && xhr.status == 200) {
		if (xhr.readyState == 4) {
			console.log("response : ", xhr.responseText);

			var listelem = document.getElementById("devices-list");

			data = xhr.responseText;
			data = data.replace(/&quot;/g, '"');
			data = JSON.parse(data);

			for(var propt in data)
			{
				// console.log("data : ", 1);
				var sublist = document.createElement('ul');
				sublist.appendChild(document.createTextNode(propt));

				console.log("propt.length : ", propt.length);
				console.log("data[propt] : ", data[propt]);

				links = data[propt]
				for(var i = 0; i < links.length; i++)
				{
					link = links[i]
					var subsublist = document.createElement('li');
					subsublist.appendChild(document.createTextNode(link));

					sublist.appendChild(subsublist);
				}

				listelem.appendChild(sublist);
			}
		}
	};
	xhr.open("GET", "https://dev.skeyecode.com/query/identities/testservice", true);
	xhr.send();
}

function notifyDevice(){

	var xhr = new XMLHttpRequest();
	var txToken;

	// callback when done
	xhr.onreadystatechange = function() {
		// if (xhr.readyState == 4 && xhr.status == 200) {
		if (xhr.readyState == 4) {
			console.log("notify response : ", xhr.responseText);
			document.getElementById("notify-response").innerHTML = xhr.responseText ;
		}
	};

	xhr.open("POST", "https://dev.skeyecode.com/device/notify", true);
	xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

	var form = document.getElementsByName("tx-create-form");
	var form0 = form[0];

	var identity = form0.elements.namedItem("identity").value; // pb form0
	console.log("gTxToken : ", gTxToken, " identity : ", identity);

	var j = {
		"txtoken": gTxToken,
		"identity": identity
	};
	xhr.send(JSON.stringify(j));
}
