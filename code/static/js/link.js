"use strict";


// GLOBAL for the result
var qrcode = new QRCode("qrcode", {
    text: gLinkToken,
    width: 128,
    height: 128,
    colorDark : "#000000",
    colorLight : "#ffffff",
    correctLevel : QRCode.CorrectLevel.H
});

// Passed by jinja template
var currentLinkToken = gLinkToken;
var renewalDate = Date.parse(gExpirationDate);

var timeToRenewalInS = (renewalDate - Date.now()) / 1000;
console.log("inititial link token : ", gLinkToken);
console.log("inititial delay : ", timeToRenewalInS);

var circleStyle = {
  strokeWidth: 30,
  easing: 'linear',
  color: '#FFEA82',
  trailColor: '#eee',
  trailWidth: 1,
  svgStyle: null
};

var bar = new ProgressBar.Circle(progresscircle, circleStyle);


function refreshLinkToken(){
  var xhr = new XMLHttpRequest();

  // callback when done
  xhr.onreadystatechange = function() {
    // if (xhr.readyState == 4 && xhr.status == 200) {
    if (xhr.readyState == 4) {
      console.log("response : ", xhr.responseText);

      var response_elem = document.getElementById("link-token");

      var data = xhr.responseText;

      try {
        data = data.replace(/&quot;/g, '"');
        data = JSON.parse(data);

        currentLinkToken = data["linktoken"]
        timeToRenewalInS = data["time_to_renewal_in_s"]
        renewalDate = Date.parse(data["expires_date"])

        response_elem.innerHTML = "Code to enter : " + currentLinkToken;

        qrcode.clear();
        qrcode.makeCode(currentLinkToken);

        // reset the progress animation
        bar.destroy();
        bar = new ProgressBar.Circle(progresscircle, circleStyle);
        bar.animate(1.0, {
            duration: timeToRenewalInS * 1000
        });
      }
      catch(err) {
          console.error("refreshLinkToken : exception : ", err);
      }

    }
  };

  // Empty POST on the current URL
  // Check if we need to renew
  if( Date.now() > renewalDate ){
    xhr.open("POST", window.location.href, true);
    xhr.send();
  } else {
    console.log("no need to renew, expires in : ", (renewalDate - Date.now()) / 1000 );
  }
}


// Token ttl is 60s
//TODO use 60*1000
setInterval(refreshLinkToken, 60 * 1000);


bar.animate(1.0, {
    duration: timeToRenewalInS * 1000
});
